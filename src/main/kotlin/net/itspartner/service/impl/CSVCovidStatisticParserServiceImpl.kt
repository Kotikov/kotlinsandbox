package net.itspartner.service.impl

import net.itspartner.model.CovidStatistic
import net.itspartner.service.CovidStatisticParserService
import org.apache.commons.csv.CSVFormat
import org.springframework.stereotype.Service
import java.io.StringReader

@Service
class CSVCovidStatisticParserServiceImpl : CovidStatisticParserService {

    override fun parse(content: String): List<CovidStatistic> {

        val csvReader = StringReader(content)
        val records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(csvReader)
        val stats = mutableListOf<CovidStatistic>()
        for (record in records) {
            val totalLatestCases = Integer.parseInt(record.get(record.size() - 1))
            val prevDayCases = Integer.parseInt(record.get(record.size() - 2))
            val covid = CovidStatistic(
                record.get("Province/State"),
                record.get("Country/Region"),
                totalLatestCases,
                totalLatestCases - prevDayCases)
            stats.add(covid)
        }

        return stats
    }
}