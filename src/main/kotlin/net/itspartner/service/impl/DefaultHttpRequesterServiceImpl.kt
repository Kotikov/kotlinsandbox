package net.itspartner.service.impl

import net.itspartner.service.HttpRequesterService
import org.springframework.stereotype.Service
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@Service
class DefaultHttpRequesterServiceImpl : HttpRequesterService {

    override fun get(url: String): String {

        val client = HttpClient.newBuilder().build()
        val request = HttpRequest.newBuilder()
            .uri(URI.create(url))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())

        return response.body()
    }

}
