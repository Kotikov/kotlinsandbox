package net.itspartner.service.impl

import net.itspartner.model.CovidStatistic
import net.itspartner.service.CovidService
import net.itspartner.service.CovidStatisticParserService
import net.itspartner.service.HttpRequesterService
import org.springframework.stereotype.Service

@Service
class CovidServiceImpl(
    private val httpRequester: HttpRequesterService,
    private val covidStatisticParser: CovidStatisticParserService
) : CovidService {

    companion object {

        private const val COVID_STATISTIC_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"
    }

    override fun getStatistics(): List<CovidStatistic> {

        val body = httpRequester.get(COVID_STATISTIC_URL)

        return covidStatisticParser.parse(body)
    }
}
