package net.itspartner.service

import net.itspartner.model.CovidStatistic

interface CovidStatisticParserService {

    fun parse(content: String): List<CovidStatistic>
}