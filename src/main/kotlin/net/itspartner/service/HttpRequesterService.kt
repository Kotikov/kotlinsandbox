package net.itspartner.service

interface HttpRequesterService {

    fun get(url: String): String
}
