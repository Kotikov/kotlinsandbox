package net.itspartner.dto

import com.fasterxml.jackson.annotation.JsonProperty
import net.itspartner.model.CovidStatistic

class CovidStatisticListDto(covidStatisticList: List<CovidStatistic>) {

    @JsonProperty("covidStatistics")
    private val covidStatistics: List<CovidStatisticDto>

    init {
        val result = mutableListOf<CovidStatisticDto>()
        covidStatisticList.forEach { cs -> result.add(CovidStatisticDto(cs)) }
        covidStatistics = result
    }

    fun getCovidStatistics(): List<CovidStatisticDto> {

        return covidStatistics
    }
}