package net.itspartner.controller

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import io.kotlintest.matchers.shouldBe
import io.kotlintest.mock.`when`
import net.itspartner.model.CovidStatistic
import net.itspartner.random.Randomizer.randomStr
import net.itspartner.service.impl.CovidServiceImpl
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.springframework.ui.ExtendedModelMap

class CovidControllerTestSpec : Spek({

    describe("#getCovidStatistic") {

        val covidService = mock<CovidServiceImpl>()

        beforeEachTest {
            reset(covidService)
        }

        on("get list of covid statistics") {

            val country = randomStr(10)
            val covidStatistic = CovidStatistic("", country, 300, 200)
            `when`(covidService.getStatistics()).thenReturn(listOf(covidStatistic))

            val controller = CovidController(covidService)

            val result = controller.getCovidStatistic(ExtendedModelMap())

            it("returns list of covid statistics") {

                result.getCovidStatistics().size shouldBe 1
            }

            it("returns covidStatistic object") {

                // dto and original entity, so compare fields
                // the verifier does not compare by equals, because dto and the original entity may have different fields
                result.getCovidStatistics()[0].country shouldBe covidStatistic.country
                result.getCovidStatistics()[0].state shouldBe covidStatistic.state
                result.getCovidStatistics()[0].diffFromPrevDay shouldBe covidStatistic.diffFromPrevDay
                result.getCovidStatistics()[0].totalLatestCases shouldBe covidStatistic.totalLatestCases
            }
        }
    }
})