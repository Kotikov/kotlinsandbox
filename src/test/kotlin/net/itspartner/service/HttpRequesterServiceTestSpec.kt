package net.itspartner.service

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import io.kotlintest.matchers.shouldBe
import net.itspartner.service.impl.DefaultHttpRequesterServiceImpl
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

internal class HttpRequesterServiceTestSpec : Spek({

    val port = 8080
    val baseUrl = "http://localhost:$port"

    describe("#get") {

        val wireMockServer = WireMockServer()

        beforeEachTest { wireMockServer.start() }

        afterEachTest { wireMockServer.stop() }

        on("valid http request") {

            val httpRequester = DefaultHttpRequesterServiceImpl()

            val body = "hello"
            stubFor(get(urlEqualTo("/hello"))
                .willReturn(aResponse()
                    .withStatus(200)
                    .withBody("hello")))

            val responseBody = httpRequester.get("$baseUrl/hello")

            it("returns response: $body") {

                responseBody shouldBe body
            }
        }
    }

})