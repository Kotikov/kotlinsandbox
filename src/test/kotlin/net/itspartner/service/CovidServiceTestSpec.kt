package net.itspartner.service

import com.nhaarman.mockito_kotlin.mock
import io.kotlintest.matchers.shouldBe
import net.itspartner.model.CovidStatistic
import net.itspartner.random.Randomizer
import net.itspartner.service.impl.CSVCovidStatisticParserServiceImpl
import net.itspartner.service.impl.CovidServiceImpl
import net.itspartner.service.impl.DefaultHttpRequesterServiceImpl
import net.itspartner.utils.FileLoaderUtils
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockito.Mockito.*

internal class CovidServiceTestSpec : Spek({

    describe("#getStatistics") {

        val httpRequester = mock<DefaultHttpRequesterServiceImpl>()
        val covidStatisticParser = mock<CSVCovidStatisticParserServiceImpl>()

        beforeEachTest {
            reset(httpRequester, covidStatisticParser)
        }

        on("http gets csv content and covid parser parses it") {

            val country = Randomizer.randomStr(5)
            `when`(httpRequester.get(anyString()))
                .thenReturn(FileLoaderUtils.readAll("src/test/resources/time_series_covid19_one_country.csv"))
            val covidStatistic = CovidStatistic("", country, 300, 200)
            `when`(covidStatisticParser.parse(anyString()))
                .thenReturn(listOf(covidStatistic))

            val covidService = CovidServiceImpl(httpRequester, covidStatisticParser)

            val result = covidService.getStatistics()

            it("returns a covid statistic list of size 1") {

                result.size shouldBe 1
            }

            it("returns covidStatistic object") {

                result[0] shouldBe covidStatistic
            }
        }
    }
})
